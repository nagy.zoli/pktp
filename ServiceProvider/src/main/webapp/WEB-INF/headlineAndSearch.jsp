<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a href="TeamServlet" class="navbar-brand">
            <img src="images/logo.png" height="28" alt="logo">
        </a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
    <a class="navbar-brand" href="DemoServlet">Home</a>
    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="LoginServlet">Login</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="RegisterServlet">Register</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="ServiceServlet">Add new service</a>
        </li>
    </ul>
    <ul class="nav navbar-nav">
        <li class="nav-item">
            <div class="search-container">
                <form method="GET" action="SearchServlet">
                    <input type="text" placeholder="Search..." name="search">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </li>        

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                <c:if test="${userDTO.name!= null}">
                    Welcome,  ${userDTO.name}
                </c:if>
                <c:if test="${userDTO.name == null}">
                    Welcome, Anonymus!
                </c:if>                
            </a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="ListServicesServlet">My services</a>
                <a class="dropdown-item" href="MessagesServlet">Message</a>
                <a class="dropdown-item" href="PasswordRecoveryServlet">Change password</a>
                <a class="dropdown-item" href="LogoutServlet">Logout</a>
            </div>
        </li>
    </ul>
</nav>

