<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <%@include file="cssForHeadLine.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
        <%@include file="head.jsp" %>
    </head>
    <body>
        <%@include file="headlineAndSearch.jsp" %>
        <c:if test="${registerError != null}">
            <div class="alert alert-danger" role="alert">
                ${registerError}
                <c:if test = "${registerError == 'User already exist.'}">
                    <a href="LoginServlet"><c:out value = "Please, log in." /></a>
                </c:if>
            </div>
        </c:if>
        <div class="py-5 text-center">
            <h2>Register form</h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="col mx-auto">
                    <form method="POST" action="RegisterServlet" class="needs-validation" novalidate>
                        <div class="mb-3">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" value="${nameAttempt}" id="name" name="name" required>
                            <div class="invalid-feedback">
                                A valid name is required.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" value="${emailAttempt}" id="email" name="email" placeholder="you@example.com" required>
                            <div class="invalid-feedback">
                                Please enter a valid email address.
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="confirm_email">Confirm email</label>
                            <input type="email" class="form-control" id="confirm_email" name="confirm_email" required>
                            <div class="invalid-feedback">
                                Please confirm email address.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                            <div class="invalid-feedback">
                                Please enter a password.
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="confirm_password">Confirm password</label>
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" required>
                            <div class="invalid-feedback">
                                Please confirm the password.
                            </div>
                        </div>

                        <hr class="mb-6">
                        <button class="btn btn-outline-dark" type="submit">Register</button>
                    </form>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/form-validation.js"></script>
    </body>
</html>
