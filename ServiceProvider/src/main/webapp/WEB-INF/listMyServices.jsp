<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="head.jsp" %>
        <%@include file="cssForHeadLine.jsp" %>
        <title>Card view</title>
        <%@include file="cssForCollapsible.jsp" %>        
    </head>
    <body> 
        <%@include file="headlineAndSearch.jsp" %>
        <c:if test="${removeError != null}">
            <div class="alert alert-danger" role="alert">
                Unable to remove service.
            </div>
        </c:if>
        <c:if test="${serviceError != null}">
            <div class="alert alert-danger" role="alert">
                ${serviceError}
            </div>
        </c:if>
        
        <br> <br>
        <div class="container">
            <div class="card-columns">
                <c:forEach items="${serviceDTOs}" var="serviceDTO">
                    <div class="card bg-light mb-3" style="width: 17rem; border-color: #777;" name="service" id="service">
                        <img class="card-img-top" src=${serviceDTO.image} alt="Card image" style="object-fit:cover;">
                        <div class="card-body">
                            <h4 class="card-title" style="font-family: serif; font-weight: bold;">${serviceDTO.name}</h4>
                            <p class="card-text">${serviceDTO.shortDescription}</p>
                            <button type="button" class="btn btn-outline-dark" id="button1">Details</button>
                            <div class="content">
                                <p>
                                    <br>
                                    ${serviceDTO.description}
                                    <br>
                                    <c:forEach items="${serviceDTO.categoryDTOs}" var="categoryDTO">                               
                                    <form action="CategoryServlet" method="get">
                                        <button class="button button4" id="category" name="category" value="${categoryDTO.name}">#${categoryDTO.name}</button>  
                                    </form>
                                </c:forEach>
                                </p>
                            </div>
                            <br> <br>
                            <form action="RemoveServiceServlet" method="get">
                                <button class="btn btn-outline-dark" id="serviceId" name="serviceId" value="${serviceDTO.id}">Remove service</button>  
                            </form>
                            <br>
                            <form action="EditServiceServlet" method="get">
                                <button class="btn btn-outline-dark" id="id" name="id" value="${serviceDTO.id}">Edit service</button>  
                            </form>                       
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <%@include file="scriptForCollapsible.jsp" %>        
    </body>
</html>
