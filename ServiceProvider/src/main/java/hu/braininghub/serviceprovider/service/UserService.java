package hu.braininghub.serviceprovider.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import hu.braininghub.serviceprovider.helper.EmailValidateHelper;
import hu.braininghub.serviceprovider.helper.NotifySamples;
import hu.braininghub.serviceprovider.mapper.UserMapper;
import hu.braininghub.serviceprovider.model.UserDAO;
import hu.braininghub.serviceprovider.model.UserDTO;
import java.sql.Date;
import java.time.LocalDate;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;

@Stateless
public class UserService {

    @Inject
    private UserDAO dao;

    @Inject
    private EmailService emailService;

    public void adduser(String email, String name, String password) throws Exception {

        if (!EmailValidateHelper.isValidEmail(email)) {
            throw new Exception("Invalid email.");
        }

        if (userExist(email)) {
            throw new Exception("User already exist.");
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setEmail(email);
        userDTO.setName(name);
        userDTO.setTimeOfRegister(Date.valueOf(LocalDate.now()));
        userDTO.setValidated(false);

        String validateCode = generateValidateCode();
        userDTO.setValidationCode(validateCode);

        String hashedpwd = BCrypt.withDefaults().hashToString(4, password.toCharArray());
        userDTO.setPassword(hashedpwd);

        dao.addUser(UserMapper.MAPPER.toEntity(userDTO));
        emailService.sendEmailNotification(userDTO.getEmail(),
                NotifySamples.USER_REGISTER_SUBJECT,
                NotifySamples.USER_REGISTER_TEXT + "\n\nYour validation code is:\n\n" + validateCode + "\n\n");
    }

    public String getUserName(String email) {

        return dao.getUser(email).getName();
    }

    public void removeUser(UserDTO userDTO) {
        dao.removeUser(UserMapper.MAPPER.toEntity(userDTO));
    }

    public void recoverPassword(String email) throws Exception {
        if (!EmailValidateHelper.isValidEmail(email)) {
            throw new Exception("Invalid email.");
        }
        
        if (userExist(email)) {

            String newPassword = generateTemporaryPassword();

            dao.updateUserPassword(email, BCrypt.withDefaults().hashToString(4, newPassword.toCharArray()));
            emailService.sendEmailNotification(email, NotifySamples.PASSWORD_RECOVERY_SUBJECT, "Your new password:\n\n" + newPassword + "\n");
        } else {
            throw new Exception("User does not exist.");
        }
    }

    public void changePassword(String email, String newPassword) throws Exception {
        if (!EmailValidateHelper.isValidEmail(email)) {
            throw new Exception("Invalid email.");
        }
        
        if (userExist(email)) {

            String newHashedPassword = BCrypt.withDefaults().hashToString(4, newPassword.toCharArray());
            dao.updateUserPassword(email, newHashedPassword);
        }
    }

    private String generateTemporaryPassword() {
        return new PasswordGenerator().generatePassword(8,
                new CharacterRule(EnglishCharacterData.Alphabetical),
                new CharacterRule(EnglishCharacterData.Digit));
    }

    private String generateValidateCode() {
        return new PasswordGenerator().generatePassword(6,
                new CharacterRule(EnglishCharacterData.Alphabetical),
                new CharacterRule(EnglishCharacterData.Digit));
    }

    private boolean userExist(String email) {

        return dao.getUser(email) != null;
    }
    
    public void cleanUpInvalidUsers() {
        
        dao.cleanUpInvalidUsers();
    }
}
