
package hu.braininghub.serviceprovider.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class ProviderDTO {
    
    private int id;
    private String email;
    private String phone;
    private String address;
    
    @EqualsAndHashCode.Exclude
    private List<ServiceDTO> serviceDTOs;

}
