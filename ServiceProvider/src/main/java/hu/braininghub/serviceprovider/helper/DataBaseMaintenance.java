package hu.braininghub.serviceprovider.helper;

import hu.braininghub.serviceprovider.service.UserService;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;


@Singleton
public class DataBaseMaintenance {
    
    @Inject
    UserService userService;
    
    @Schedule(dayOfWeek = "Sun", hour = "23", persistent = false)
    private void clearInvalidUsers(){
        userService.cleanUpInvalidUsers();
    }  
}
