package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.LoginService;
import java.io.IOException;
import java.util.Optional;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    @Inject
    private LoginService loginService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserDTO user = SessionHelper.getUserFromSession(request.getSession());
        //Optional<UserDTO> user = SessionHelper.getUserFromSession(request.getSession());

        if (user == null) {
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + "/DemoServlet");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            request.setAttribute("loginAttempt", request.getParameter("email"));
            Optional<String> validationCode = Optional.ofNullable(request.getParameter("validationCode"));
            
            UserDTO user = loginService.userLogin(request.getParameter("email"), request.getParameter("password"), validationCode.orElse(""));
            
            SessionHelper.setUserToSession(request.getSession(), user);
            response.sendRedirect(request.getContextPath() + "/DemoServlet");
        } catch (Exception ex) {
            request.setAttribute("loginError", ex.getMessage());
            
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        }
    }
}
