package hu.braininghub.serviceprovider.servlet;

import hu.braininghub.serviceprovider.helper.SessionHelper;
import hu.braininghub.serviceprovider.model.ProviderDTO;
import hu.braininghub.serviceprovider.model.ServiceDTO;
import hu.braininghub.serviceprovider.model.UserDTO;
import hu.braininghub.serviceprovider.service.ProviderService;
import hu.braininghub.serviceprovider.service.ServiceService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.LoggerFactory;

@WebServlet(name = "ListServicesServlet", urlPatterns = {"/ListServicesServlet"})
public class ListServicesServlet extends HttpServlet {
    
    @Inject
    private ServiceService serviceService;
    
    @Inject
    private ProviderService providerService;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        UserDTO loggedInUser = SessionHelper.getUserFromSession(session);
        
        if (loggedInUser == null) {
            response.sendRedirect("LoginServlet");
        } else {
            
            String providerEmail = loggedInUser.getEmail();
            ProviderDTO providerDTO = providerService.getProviderByEmail(providerEmail);
            List<ServiceDTO> serviceDTOs = null;
            
            try {
                serviceDTOs = serviceService.getServicesOfProvider(providerDTO);
                for (ServiceDTO dto : serviceDTOs) {
                    if (dto.getImage().equals("")) {
                        dto.setImage("images/placeholder.jpg");
                    }
                }
                request.setAttribute("serviceDTOs", serviceDTOs);
                request.setAttribute("serviceError", session.getAttribute("serviceError"));
                session.removeAttribute("serviceError");
                request.setAttribute("removeError", session.getAttribute("removeError"));
                session.removeAttribute("removeError");
            } catch (SQLException ex) {
                LoggerFactory.getLogger(this.getClass().getName()).error("An error occurred {}", ex.getMessage());
            }
            
            if (serviceDTOs.isEmpty()) {
                request.getRequestDispatcher("WEB-INF/noServices.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("WEB-INF/listMyServices.jsp").forward(request, response);
            }
        }
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    
}
